import './App.css';
import Table from './component/Table/Table';
import AppBar from './component/AppBar/index';

function App() {
  return (
    <div className="app">
    <AppBar/>
     <Table/>
    </div>
  );
}

export default App;
