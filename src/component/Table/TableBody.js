import React from 'react'

function TableBody(props) {
    return (
        <tr className="tbody-row">
            {/* <td className='tbody-content'>{index + 1}</td> */}
            <td className='tbody-content'>{props.data.name}</td>
            <td className='tbody-content'>{props.data.Author}</td>
            <td className='tbody-content'>{props.data.ISBN}</td>
        </tr>
    )
}

export default TableBody