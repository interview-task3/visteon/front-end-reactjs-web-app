import React from 'react'

function TableHeading({heading}) {
    return (
        <tr>
            {/* <th>ID</th> */}
            <th>{heading[0]}</th>
            <th>{heading[1]}</th>
            <th>{heading[2]}</th>
        </tr>
    )
}

export default TableHeading