import { Box, CircularProgress } from '@mui/material';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import TableBody from './TableBody';
import TableHeading from './TableHeading';


function Table() {
    let size = 4
    const [tableData, settableData] = useState([]);
    const [page, setpage] = useState(1);
    const [totalCount, settotalCount] = useState(0);
    const [TotalPageCount, setTotalPageCount] = useState(0)
    const [Loading, setLoading] = useState(true)


    const baseURL = `https://nodejs-api-page.herokuapp.com/api/books/getBooks?page=${page}&size=${size}`;



    useEffect(() => {

        axios.get(baseURL).then((response) => {
            settableData(response.data.data);
            settotalCount(response.data.totalCount)
            // setTotalPageCount(calculatePagesCount(size, totalCount));

            setLoading(false)

        });
    }, [page, baseURL]);
    const calculatePagesCount = (pageSize, totalCount) => {
        // we suppose that if we have 0 items we want 1 empty page
        return totalCount < pageSize ? 1 : Math.ceil(totalCount / pageSize);
    };

    const nextOrPrevious = (params) => {
        setTotalPageCount(calculatePagesCount(size, totalCount));

        if (params === "First") {
            setpage(1)
        }
        if (params === "Prev") {

            setpage(page - 1)
        }
        if (params === "Next") {
            setpage(page + 1)
        }

        if (params === "Last") {
            setTotalPageCount(calculatePagesCount(size, totalCount));
            let lastPage = calculatePagesCount(size, totalCount);
            setpage(lastPage)
        }

    }

    const heading = ["name", "author", "ISBU"]



    return (
        <div className='main'>

            <div className='table-div'>
                {
                    Loading ?
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <CircularProgress />
                        </div>
                        :
                        <>
                            <table className='table'>
                                <thead className='table-head'>
                                    <TableHeading heading={heading} />
                                </thead>
                                <tbody className='tbody'>
                                    {
                                        tableData && tableData.map((data, index) =>
                                        (
                                            <TableBody key={index} data={data} />
                                        ))
                                    }
                                </tbody>

                            </table>
                            <div className='table-footer-section'>
                                <div className='button-section'>
                                    <button className='btn' onClick={() => { nextOrPrevious("First") }}>First</button>
                                    <button disabled={page === 0 ? true : false} className='btn' onClick={() => { nextOrPrevious("Prev") }}>Prev</button>
                                    <button disabled={page === TotalPageCount} className='btn' onClick={() => { nextOrPrevious("Next") }}>Next</button>
                                    <button disabled={page === TotalPageCount} className='btn' onClick={() => { nextOrPrevious("Last") }}>Last</button>
                                </div>
                                <div>total of {totalCount} records</div>
                            </div>
                        </>

                }


            </div>
        </div>
    )
}

export default Table